package com.luismmt.dogedex

import com.luismmt.dogedex.api.ApiResponseStatus
import com.luismmt.dogedex.api.ApiService
import com.luismmt.dogedex.api.dto.AddDogToUserDTO
import com.luismmt.dogedex.api.dto.DogDTO
import com.luismmt.dogedex.api.dto.LogInDTO
import com.luismmt.dogedex.api.dto.SignUpDTO
import com.luismmt.dogedex.api.responses.*
import com.luismmt.dogedex.dogList.DogListRepository
import com.luismmt.dogedex.model.Dog
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.Assert.*

import org.junit.Test
import java.net.UnknownHostException

class DogRepositoryTest {


    @Test
    fun testGetDogCollectionSucces(): Unit = runBlocking {

        class fakeApiService : ApiService {
            override suspend fun getAllDogs(): DogListApiResponse {
                return DogListApiResponse(
                    message = "", isSuccess = true, data = DogList(
                        dogs = listOf(
                            DogDTO(
                                1, "1", "", "", "", 0, "", "", "", "", "", "", "", "", "", ""
                            ),
                            DogDTO(
                                19,
                                "2",
                                "",
                                "",
                                "",
                                2,
                                "",
                                "",
                                "Charmeleon",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                ""
                            ),
                        )
                    )
                )
            }

            override suspend fun signUp(signUpDTO: SignUpDTO): AuthApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun signIn(LogInDTO: LogInDTO): AuthApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun addDogToUSer(addDogToUserDTO: AddDogToUserDTO): DefaultResponse {
                TODO("Not yet implemented")
            }

            override suspend fun getUserDogs(): DogListApiResponse {
                return DogListApiResponse(
                    message = "", isSuccess = true, data = DogList(
                        dogs = listOf(
                            DogDTO(
                                19,
                                "2",
                                "Charmeleon",
                                "",
                                "",
                                2,
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                ""
                            ),
                        )
                    )
                )
            }

            override suspend fun getDogByMLId(mlId: String): DogApiResponse {
                TODO("Not yet implemented")
            }

        }

        val dogRepository = DogListRepository(
            apiService = fakeApiService(), dispatcher = TestCoroutineDispatcher()
        )
        val apiResponseStatus = dogRepository.getDogCollection()

        assert(apiResponseStatus is ApiResponseStatus.SUCCESS)
        val dogDollection = (apiResponseStatus as ApiResponseStatus.SUCCESS).data
        assertEquals(2, dogDollection.size)


        assertEquals("Charmeleon", dogDollection[1].name)
        assertEquals("", dogDollection[0].name)
    }


    @Test
    fun testGetAllDogError(): Unit = runBlocking {

        class fakeApiService : ApiService {
            override suspend fun getAllDogs(): DogListApiResponse {
                throw UnknownHostException()
            }

            override suspend fun signUp(signUpDTO: SignUpDTO): AuthApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun signIn(LogInDTO: LogInDTO): AuthApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun addDogToUSer(addDogToUserDTO: AddDogToUserDTO): DefaultResponse {
                TODO("Not yet implemented")
            }

            override suspend fun getUserDogs(): DogListApiResponse {
                return DogListApiResponse(
                    message = "", isSuccess = true, data = DogList(
                        dogs = listOf(
                            DogDTO(
                                19,
                                "2",
                                "Charmeleon",
                                "",
                                "",
                                2,
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                ""
                            ),
                        )
                    )
                )
            }

            override suspend fun getDogByMLId(mlId: String): DogApiResponse {
                TODO("Not yet implemented")
            }

        }

        val dogRepository = DogListRepository(
            apiService = fakeApiService(), dispatcher = TestCoroutineDispatcher()
        )
        val apiResponseStatus = dogRepository.getDogCollection()

        assert(apiResponseStatus is ApiResponseStatus.ERROR)

        assertEquals(
            R.string.unknow_exception_error,
            (apiResponseStatus as ApiResponseStatus.ERROR).messageId
        )

    }

    @Test
    fun getDogByMlSucces(): Unit = runBlocking {
        class fakeApiService : ApiService {
            override suspend fun getAllDogs(): DogListApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun signUp(signUpDTO: SignUpDTO): AuthApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun signIn(LogInDTO: LogInDTO): AuthApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun addDogToUSer(addDogToUserDTO: AddDogToUserDTO): DefaultResponse {
                TODO("Not yet implemented")
            }

            override suspend fun getUserDogs(): DogListApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun getDogByMLId(mlId: String): DogApiResponse {
                return DogApiResponse(
                    message = "", isSuccess = true, data = DogResponse(
                        dog = DogDTO(
                            19, "2", "Charmeleon", "", "", 2, "", "", "", "", "", "", "", "", "", ""
                        ),
                    )
                )
            }

        }

        val dogRepository = DogListRepository(
            apiService = fakeApiService(), dispatcher = TestCoroutineDispatcher()
        )
        val apiResponseStatus = dogRepository.getDogByMlId("de")
        assert(apiResponseStatus is ApiResponseStatus.SUCCESS)
        assertEquals(19L, (apiResponseStatus as ApiResponseStatus.SUCCESS).data.id)
    }

    @Test
    fun getDogByMlError(): Unit = runBlocking {
        class fakeApiService : ApiService {
            override suspend fun getAllDogs(): DogListApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun signUp(signUpDTO: SignUpDTO): AuthApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun signIn(LogInDTO: LogInDTO): AuthApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun addDogToUSer(addDogToUserDTO: AddDogToUserDTO): DefaultResponse {
                TODO("Not yet implemented")
            }

            override suspend fun getUserDogs(): DogListApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun getDogByMLId(mlId: String): DogApiResponse {
                return DogApiResponse(
                    message = "error_getting_dog_by_ml_id", isSuccess = false, data = DogResponse(
                        dog = DogDTO(
                            19, "2", "Charmeleon", "", "", 2, "", "", "", "", "", "", "", "", "", ""
                        ),
                    )
                )
            }

        }

        val dogRepository = DogListRepository(
            apiService = fakeApiService(), dispatcher = TestCoroutineDispatcher()
        )
        val apiResponseStatus = dogRepository.getDogByMlId("de")
        assert(apiResponseStatus is ApiResponseStatus.ERROR)
        assertEquals(R.string.unKnow_error, (apiResponseStatus as ApiResponseStatus.ERROR).messageId)
    }
}