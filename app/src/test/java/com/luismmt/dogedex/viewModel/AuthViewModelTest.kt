package com.luismmt.dogedex.viewModel

import com.luismmt.dogedex.R
import com.luismmt.dogedex.api.ApiResponseStatus
import com.luismmt.dogedex.auth.AuthTask
import com.luismmt.dogedex.auth.AuthViewModel
import com.luismmt.dogedex.model.User
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test

class AuthViewModelTest {

    @get:Rule
    var dogedexCoroutineRule = DogeDexCoroutineRule()

    @Test
    fun testLoginValidationCorrect() {
        class fakeAuthRepository : AuthTask {
            override suspend fun Login(email: String, password: String): ApiResponseStatus<User> {
                return ApiResponseStatus.SUCCESS(
                    User(1, "lumt90@gmail.com", "")
                )
            }

            override suspend fun singUp(
                email: String,
                password: String,
                confirmPassword: String
            ): ApiResponseStatus<User> {
                return ApiResponseStatus.SUCCESS(
                    User(1, "", "")
                )
            }
        }

        val viewModel = AuthViewModel(authRepository = fakeAuthRepository())

        viewModel.login("", "test")

        assertEquals(R.string.email_is_not_valid, viewModel.emailError.value)

        viewModel.login("lumt90@gmail.com", "")
        assertEquals(R.string.password_must_not_be_empty, viewModel.passwordError.value)


    }

    @Test
    fun testLoginStateCorrect() {

        val fakeUser = User(1, "lumt90@gmail.com", "")

        class fakeAuthRepository : AuthTask {
            override suspend fun Login(email: String, password: String): ApiResponseStatus<User> {
                return ApiResponseStatus.SUCCESS(
                    fakeUser
                )
            }

            override suspend fun singUp(
                email: String,
                password: String,
                confirmPassword: String
            ): ApiResponseStatus<User> {
                return ApiResponseStatus.SUCCESS(
                    User(1, "", "")
                )
            }
        }

        val viewModel = AuthViewModel(authRepository = fakeAuthRepository())

        viewModel.login("lumt90@gmail.com", "luismiguel")
        assertEquals(fakeUser.email, viewModel.user.value?.email)


    }

    @Test
    fun resetErrorCorrect() {


        val fakeUser = User(1, "lumt90@gmail.com", "")

        class fakeAuthRepository : AuthTask {
            override suspend fun Login(email: String, password: String): ApiResponseStatus<User> {
                return ApiResponseStatus.SUCCESS(
                    fakeUser
                )
            }

            override suspend fun singUp(
                email: String,
                password: String,
                confirmPassword: String
            ): ApiResponseStatus<User> {
                return ApiResponseStatus.SUCCESS(
                    User(1, "", "")
                )
            }
        }

        val viewModel = AuthViewModel(authRepository = fakeAuthRepository())

        viewModel.emailError.value = R.string.email_is_not_valid
        viewModel.passwordError.value = R.string.password_must_not_be_empty
        viewModel.confirmPasswordError.value = R.string.password_must_not_be_empty
        viewModel.resetError()
        assert(viewModel.emailError.value == null)
        assert(viewModel.passwordError.value == null)
        assert(viewModel.confirmPasswordError.value == null)


    }


}