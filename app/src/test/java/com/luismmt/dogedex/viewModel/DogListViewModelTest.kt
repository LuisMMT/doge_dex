package com.luismmt.dogedex.viewModel

import com.luismmt.dogedex.R
import com.luismmt.dogedex.api.ApiResponseStatus
import com.luismmt.dogedex.dogList.DogListViewModel
import com.luismmt.dogedex.dogList.DogTask
import com.luismmt.dogedex.model.Dog
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test


class DogListViewModelTest {

    @get:Rule
    var dogedexCoroutineRule = DogeDexCoroutineRule()

    @Test
    fun downloadDogListStatusCorrect() {
        class fakeDogRepository : DogTask {
            override suspend fun getDogCollection(): ApiResponseStatus<List<Dog>> {

                return ApiResponseStatus.SUCCESS(
                    listOf(
                        Dog(
                            1,
                            "",
                            "",
                            "",
                            "",
                            1,
                            "",
                            "",
                            "fake name",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            false
                        ),
                        Dog(
                            5,
                            "",
                            "",
                            "",
                            "",
                            2,
                            "",
                            "",
                            "fake name 2",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            false
                        )
                    )
                )
            }

            override suspend fun addDogToUser(dogId: Long): ApiResponseStatus<Any> {
                return ApiResponseStatus.SUCCESS(Unit)
            }

            override suspend fun getDogByMlId(mlDogID: String): ApiResponseStatus<Dog> {
                return ApiResponseStatus.SUCCESS(
                    Dog(
                        1,
                        "",
                        "",
                        "",
                        "",
                        1,
                        "",
                        "",
                        "fake name",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        false
                    )
                )
            }

        }

        val viewModel = DogListViewModel(
            dogListRepository = fakeDogRepository()
        )

        assertEquals(2, viewModel.dogList.value.size)
        assertEquals(5, viewModel.dogList.value[1].id)
        assert(viewModel.status.value is ApiResponseStatus.SUCCESS)
    }


    @Test
    fun downloadDogListErrorStatusCorrect() {
        class fakeDogRepository : DogTask {
            override suspend fun getDogCollection(): ApiResponseStatus<List<Dog>> {

                return ApiResponseStatus.ERROR(
                    R.string.unKnow_error
                )
            }

            override suspend fun addDogToUser(dogId: Long): ApiResponseStatus<Any> {
                return ApiResponseStatus.SUCCESS(Unit)
            }

            override suspend fun getDogByMlId(mlDogID: String): ApiResponseStatus<Dog> {
                return ApiResponseStatus.SUCCESS(
                    Dog(
                        1,
                        "",
                        "",
                        "",
                        "",
                        1,
                        "",
                        "",
                        "fake name",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        false
                    )
                )
            }

        }

        val viewModel = DogListViewModel(
            dogListRepository = fakeDogRepository()
        )

        assertEquals(0, viewModel.dogList.value.size)
        assert(viewModel.status.value is ApiResponseStatus.ERROR)
    }


 @Test
    fun resetSStatusCorrect() {
        class fakeDogRepository : DogTask {
            override suspend fun getDogCollection(): ApiResponseStatus<List<Dog>> {

                return ApiResponseStatus.ERROR(
                    R.string.unKnow_error
                )
            }

            override suspend fun addDogToUser(dogId: Long): ApiResponseStatus<Any> {
                return ApiResponseStatus.SUCCESS(Unit)
            }

            override suspend fun getDogByMlId(mlDogID: String): ApiResponseStatus<Dog> {
                return ApiResponseStatus.SUCCESS(
                    Dog(
                        1,
                        "",
                        "",
                        "",
                        "",
                        1,
                        "",
                        "",
                        "fake name",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        false
                    )
                )
            }

        }

        val viewModel = DogListViewModel(
            dogListRepository = fakeDogRepository()
        )


        assert(viewModel.status.value is ApiResponseStatus.ERROR)
        viewModel.resetApiResponseStatus()
     assert(viewModel.status.value==null)
    }

}