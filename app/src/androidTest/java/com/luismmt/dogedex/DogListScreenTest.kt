package com.luismmt.dogedex

import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createComposeRule
import com.luismmt.dogedex.api.ApiResponseStatus
import com.luismmt.dogedex.api.ApiService
import com.luismmt.dogedex.dogList.DogListScreen
import com.luismmt.dogedex.dogList.DogListViewModel
import com.luismmt.dogedex.dogList.DogTask
import com.luismmt.dogedex.model.Dog
import org.junit.Rule
import org.junit.Test


@OptIn(ExperimentalMaterialApi::class)
class DogListScreenTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun progressBarShowsWhenLoadingState() {
        class fakeDogRepository : DogTask {
            override suspend fun getDogCollection(): ApiResponseStatus<List<Dog>> {
                return ApiResponseStatus.LOADING()
            }

            override suspend fun addDogToUser(dogId: Long): ApiResponseStatus<Any> {
                TODO("Not yet implemented")
            }

            override suspend fun getDogByMlId(mlDogID: String): ApiResponseStatus<Dog> {
                TODO("Not yet implemented")
            }

        }

        val viewModel = DogListViewModel(
            dogListRepository = fakeDogRepository()
        )

        composeTestRule.setContent {
            DogListScreen(
                onNavigationIconClick = { /*TODO*/ },
                onDogCLicked = {/*TODO*/ },
                viewModel = viewModel
            )
        }
        composeTestRule.onNodeWithTag(testTag = "loading-wheel").assertIsDisplayed()


    }

    @Test
    fun errordialogShowIfErrorGettingDogs() {
        class fakeDogRepository : DogTask {
            override suspend fun getDogCollection(): ApiResponseStatus<List<Dog>> {
                return ApiResponseStatus.ERROR(messageId = R.string.there_was_an_error)
            }

            override suspend fun addDogToUser(dogId: Long): ApiResponseStatus<Any> {
                TODO("Not yet implemented")
            }

            override suspend fun getDogByMlId(mlDogID: String): ApiResponseStatus<Dog> {
                TODO("Not yet implemented")
            }

        }

        val viewModel = DogListViewModel(
            dogListRepository = fakeDogRepository()
        )

        composeTestRule.setContent {
            DogListScreen(
                onNavigationIconClick = { /*TODO*/ },
                onDogCLicked = {/*TODO*/ },
                viewModel = viewModel
            )
        }
        // depende del idioma del celular
        composeTestRule.onNodeWithText(text = "there was an error").assertIsDisplayed()
        composeTestRule.onNodeWithTag(testTag = "error-dialog").assertIsDisplayed()


    }

    @Test
    fun dogListShowIfSuccessGettingDogs() {
        val dogName1 ="fake name"
        val dogName2 ="fake name 2"
        class fakeDogRepository : DogTask {

            override suspend fun getDogCollection(): ApiResponseStatus<List<Dog>> {
                return ApiResponseStatus.SUCCESS(
                    listOf(
                        Dog(
                            1,
                            "",
                            "",
                            "",
                            "",
                            1,
                            "",
                            "",
                            dogName1,
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            true
                        ),
                        Dog(
                            5,
                            "",
                            "",
                            "",
                            "",
                            23,
                            "",
                            "",
                            dogName2,
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            false
                        )
                    )
                )
            }

            override suspend fun addDogToUser(dogId: Long): ApiResponseStatus<Any> {
                TODO("Not yet implemented")
            }

            override suspend fun getDogByMlId(mlDogID: String): ApiResponseStatus<Dog> {
                TODO("Not yet implemented")
            }

        }

        val viewModel = DogListViewModel(
            dogListRepository = fakeDogRepository()
        )

        composeTestRule.setContent {
            DogListScreen(
                onNavigationIconClick = { /*TODO*/ },
                onDogCLicked = {/*TODO*/ },
                viewModel = viewModel
            )
        }


    composeTestRule.onNodeWithTag(useUnmergedTree = true, testTag = "dog-${dogName1}").assertIsDisplayed()
    composeTestRule.onNodeWithText(text = "23").assertIsDisplayed()


    }
}