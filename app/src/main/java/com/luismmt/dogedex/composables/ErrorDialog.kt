package com.luismmt.dogedex.composables

import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.testTag
import com.luismmt.dogedex.R
import com.luismmt.dogedex.api.ApiResponseStatus


@Composable
fun ErrorDialog(
    message: Int,
    onErrorDialogDismiss: () -> Unit,
) {
    AlertDialog(
        modifier = Modifier.semantics {testTag ="error-dialog" },
        onDismissRequest = {},
        title = {
            Text(text = "Oops something error")
        },
        text = { Text(stringResource(id = message)) },
        confirmButton = {
            Button(onClick = { onErrorDialogDismiss() }) {
                Text(text = stringResource(id = R.string.try_again))
            }
        })
}