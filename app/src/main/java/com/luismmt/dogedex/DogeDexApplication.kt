package com.luismmt.dogedex

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class DogeDexApplication :Application() {

}