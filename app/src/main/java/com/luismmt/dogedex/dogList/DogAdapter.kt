package com.luismmt.dogedex.dogList

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnLongClickListener
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.luismmt.dogedex.R
import com.luismmt.dogedex.databinding.DogListItemBinding
import com.luismmt.dogedex.model.Dog


class DogAdapter : ListAdapter<Dog, DogAdapter.DogViewHolder>(DiffCallback) {
    companion object DiffCallback : DiffUtil.ItemCallback<Dog>() {
        override fun areItemsTheSame(oldItem: Dog, newItem: Dog): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Dog, newItem: Dog): Boolean {
            return oldItem.id == newItem.id
        }
    }

    //permite el click a cada item
    private var onItemClickListener: ((Dog) -> Unit)? = null
    fun myDogClickListener(onItemClickListener: (Dog) -> Unit) {
        this.onItemClickListener = onItemClickListener
    }

    //permite el click largo a cada item
    /* private var onLongItemClickListener: ((Dog) -> Unit)? = null
    fun setLongOnClickListener(onLongClickListener: (Dog) -> Unit) {
         this.onLongItemClickListener = onLongClickListener
     }*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DogViewHolder {
        val binding = DogListItemBinding.inflate(LayoutInflater.from(parent.context))
        return DogViewHolder(binding)
    }


    override fun onBindViewHolder(holder: DogViewHolder, position: Int) {
        val dog = getItem(position)
        holder.bind(dog)
    }

    override fun getItemId(position: Int): Long {
        return position as Long
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class DogViewHolder(private val binding: DogListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(dog: Dog) {
            if (dog.inCollection) {
                binding.dogName.text = dog.name
                binding.dogId.visibility = View.GONE

                // al dar click al dogName llama la accion de click
                binding.dogListItem.setOnClickListener {
                    onItemClickListener?.invoke(dog)
                }
                /*binding.dogListItem.setOnLongClickListener {
                    onLongItemClickListener?.invoke(dog)
                    true
                }*/

                binding.dogImage.load(dog.image_url)

            } else {

                binding.dogImage.background = ContextCompat.getDrawable(
                    binding.dogImage.context, R.drawable.dog_list_background
                )
                binding.dogName.text = dog.name
                "${dog.index}".also { binding.dogId.text = it }
                binding.dogImage.background = ContextCompat.getDrawable(
                    binding.dogImage.context, R.drawable.dog_list_item_null_background
                )
            }


        }

    }
}

