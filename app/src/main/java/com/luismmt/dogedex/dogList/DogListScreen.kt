package com.luismmt.dogedex.dogList

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberAsyncImagePainter
import com.luismmt.dogedex.R
import com.luismmt.dogedex.api.ApiResponseStatus
import com.luismmt.dogedex.composables.BackNavigationIcon
import com.luismmt.dogedex.composables.ErrorDialog
import com.luismmt.dogedex.composables.loadingWheel
import com.luismmt.dogedex.model.Dog
import androidx.compose.material.Text
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.testTag


/** constante para gridLayout */
private const val GRID_SPAN_COUNT = 3

@ExperimentalMaterialApi
@Composable
fun DogListScreen(
    onNavigationIconClick: () -> Unit,
    onDogCLicked: (Dog) -> Unit,
    viewModel: DogListViewModel = hiltViewModel() // hiltViewModel permite hacer la inyeccion de dependencia, inyecta DogListViewModel en el constructor de DogListScreen
) {
    val status = viewModel.status.value
    val dogList = viewModel.dogList.value
    Scaffold(topBar = { DogListScreenTopBar(onNavigationIconClick) }) {
        LazyVerticalGrid(columns = GridCells.Fixed(GRID_SPAN_COUNT),
            content = {
            items(dogList) {
                DogGridItem(dog = it, onDogCLicked)
            }
        })
    }
    if (status is ApiResponseStatus.LOADING) {
        loadingWheel()
    } else if (status is ApiResponseStatus.ERROR) {
        ErrorDialog(status.messageId) { viewModel.resetApiResponseStatus() }
    }


}

@Composable
fun DogListScreenTopBar(onClick: () -> Unit) {

    TopAppBar(title = { Text(text = stringResource(R.string.my_dog_collection)) },
        backgroundColor = Color.White,
        contentColor = Color.Black,
        navigationIcon = { BackNavigationIcon(onClick) })
}


@ExperimentalMaterialApi
@Composable
fun DogGridItem(dog: Dog, onDogCLicked: (Dog) -> Unit) {
    if (dog.inCollection) {
        Surface(
            modifier = Modifier
                .padding(8.dp)
                .height(100.dp)
                .width(100.dp),
            onClick = { onDogCLicked(dog) },
            shape = RoundedCornerShape(4.dp)
        ) {
            Image(
                painter = rememberAsyncImagePainter(model = dog.image_url),
                contentDescription = "",
                modifier = Modifier.background(Color.White).semantics { testTag = "dog-${dog.name}" }
            )
        }
    } else {
        Surface(
            modifier = Modifier
                .padding(8.dp)
                .height(100.dp)
                .width(100.dp),
            color = Color.Red,
            shape = RoundedCornerShape(4.dp)
        ) {
            Text(
                text = dog.index.toString(),
                color = Color.White,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp),
                textAlign = TextAlign.Center,
                fontSize = 42.sp,
                fontWeight = FontWeight.Black
            )
        }
    }

}

@Composable
fun DogItem(dog: Dog, onDogCLicked: (Dog) -> Unit) {
    if (dog.inCollection) {
        Text(
            modifier = Modifier
                .padding(16.dp)
                .clickable { onDogCLicked(dog) }, text = dog.name
        )
    } else {
        Text(
            modifier = Modifier
                .padding(16.dp)
                .background(color = Color.Red), text = stringResource(
                id = R.string.dog_index_format, dog.index
            )
        )
    }


}