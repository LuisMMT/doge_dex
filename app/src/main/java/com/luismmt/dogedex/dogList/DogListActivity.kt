package com.luismmt.dogedex.dogList

import android.content.Intent

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.ExperimentalMaterialApi
import com.luismmt.dogedex.dogDetail.DogDetailComposeActivity
import com.luismmt.dogedex.dogDetail.ui.theme.DogeDexTheme
import com.luismmt.dogedex.model.Dog
import dagger.hilt.android.AndroidEntryPoint
/**
 * Se agrega dependecy inyection
 * En este ejemplo se pasa el viewModel por medio de hilt en el screen q se usa jetpack compose
 * **/
/** constante para gridLayout */
private const val GRID_SPAN_COUNT = 3

@ExperimentalMaterialApi
@AndroidEntryPoint // permite agregar dependencia de viewModel en el activity por medio de hilt
class DogListActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DogeDexTheme {
                DogListScreen(
                    onNavigationIconClick = ::onNavigationIconClick,
                    onDogCLicked = ::openDogDetailActivity,
                )
            }
        }

    }

    private fun onNavigationIconClick() {
        finish()
    }

    private fun openDogDetailActivity(dog: Dog) {
        val intent = Intent(this, DogDetailComposeActivity::class.java)
        intent.putExtra(DogDetailComposeActivity.DOG_KEY, dog)
        startActivity(intent)

    }
}