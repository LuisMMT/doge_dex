package com.luismmt.dogedex.dogList

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.luismmt.dogedex.model.Dog
import com.luismmt.dogedex.api.ApiResponseStatus
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DogListViewModel @Inject constructor(
    private val dogListRepository:DogTask
) : ViewModel() {

    /**  variable privada que captura respuesta del api*/
    var dogList = mutableStateOf<List<Dog>>(listOf())
        private set

    /**  variable privada que captura respuesta del api*/
    var status = mutableStateOf<ApiResponseStatus<Any>?>(null)
        private set



    /** metodo que inicia cuando se hace llamada al viewModel viewModel */
    init {
        /** llama al metodo que va a obtener la lista de dog
         * por coleccion guardada y el resto que falta por guardar */
        getDogCollection()
    }

    /** retorna los dogs guardado junto con los dogs que faltan */
    private fun getDogCollection() {
        viewModelScope.launch {
            status.value = ApiResponseStatus.LOADING()
            handleResponseStatus(dogListRepository.getDogCollection())
        }
    }

    /**Agrega dog al usuario*/
    fun addDogToUser(dogId: Long) {
        viewModelScope.launch {
            status.value = ApiResponseStatus.LOADING()
            handleAddDogToUserResponseStatus(dogListRepository.addDogToUser(dogId))
        }
    }

    private fun handleAddDogToUserResponseStatus(apiResponseStatus: ApiResponseStatus<Any>) {
        if (apiResponseStatus is ApiResponseStatus.SUCCESS) {
            getDogCollection()
        }
        status.value = apiResponseStatus

    }


   /* private fun downloadUserDogs() {
        viewModelScope.launch {
            status.value = ApiResponseStatus.LOADING()
            handleResponseStatus(dogListRepository.getUserDogs())
        }
    }*/

    /** se crea  coroutina para llamar al repository que devuelve la lista de dogs */
/*
    private fun downloadDog() {
        viewModelScope.launch {
            *//** Muestra el progressBar *//*
            status.value = ApiResponseStatus.LOADING()
            *//** Retorna Lista de Dogs del api previamente transformado del DTOMapper a model Domain *//*
            handleResponseStatus(dogListRepository.downloadDogs())
        }
    }*/

    @Suppress("UNCHECKED_CAST")
    private fun handleResponseStatus(apiResponseStatus: ApiResponseStatus<List<Dog>>) {
        /** evalua si el  response es succes del api*/
        if (apiResponseStatus is ApiResponseStatus.SUCCESS) {
            /** retorna dogList si es succes */
            dogList.value = apiResponseStatus.data!!
        }
        /** retorna el valor del apiResponseStatus*/
        status.value = apiResponseStatus as ApiResponseStatus<Any>
    }

    fun resetApiResponseStatus() {
        status.value = null
    }
}