package com.luismmt.dogedex.dogList

import com.luismmt.dogedex.R
import com.luismmt.dogedex.model.Dog
import com.luismmt.dogedex.api.ApiResponseStatus
import com.luismmt.dogedex.api.ApiService
import com.luismmt.dogedex.api.dto.AddDogToUserDTO
import com.luismmt.dogedex.api.dto.DogDTOMapper
import com.luismmt.dogedex.api.makeNetworkCall
import kotlinx.coroutines.*
import javax.inject.Inject

interface DogTask {
    suspend fun getDogCollection(): ApiResponseStatus<List<Dog>>
    suspend fun addDogToUser(dogId: Long): ApiResponseStatus<Any>
    suspend fun getDogByMlId(mlDogID: String): ApiResponseStatus<Dog>
}

class DogListRepository @Inject constructor(
    private val apiService: ApiService,
    private val dispatcher: CoroutineDispatcher,
) : DogTask {

    /**
     * retorna lista de perror*/
    override suspend fun getDogCollection(): ApiResponseStatus<List<Dog>> {
        return withContext(dispatcher) {
            /**deferred : no espera que inice el segundo proceso
             * sino que espera a que todos esten listos*/
            val allDogsListDeferred = async { downloadDogs() }
            val userDogsListDeferred = async { getUserDogs() }

            val allDogsListResponse = allDogsListDeferred.await()
            val userDogsListResponse = userDogsListDeferred.await()


            if (allDogsListResponse is ApiResponseStatus.ERROR) {
                allDogsListResponse
            } else if (userDogsListResponse is ApiResponseStatus.ERROR) {
                userDogsListResponse
            } else if (allDogsListResponse is ApiResponseStatus.SUCCESS
                && userDogsListResponse is ApiResponseStatus.SUCCESS
            ) {
                ApiResponseStatus
                    .SUCCESS(
                        getCollectionList(allDogsListResponse.data, userDogsListResponse.data)
                    )

            } else {
                ApiResponseStatus.ERROR(R.string.unKnow_error)
            }
        }
    }


    private fun getCollectionList(allDogList: List<Dog>, userDogList: List<Dog>): List<Dog> {
        return allDogList.map {


            val contain = userDogList.all { entry -> entry.id != it.id }
            if (contain) {
                Dog(
                    it.id,
                    "",
                    "",
                    "",
                    "",
                    it.index,
                    "",
                    "",
                    it.name,
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    false
                )
            } else {
                it
            }
        }.sorted()
    }


    /**
     * se crea un funcion tipo suspend porque esta dentro de una coroutina
     * Metodo llamar al api ListDogs
     */
    suspend fun downloadDogs(): ApiResponseStatus<List<Dog>> {
        return makeNetworkCall {
            /** llama al api, infla resultado en la variable dogListApiResponse  */
            val dogListApiResponse = apiService.getAllDogs()

            /** obtiene de la respuesta la lista de Dogs de tipo DTO  */
            val dogDTOList = dogListApiResponse.data.dogs

            /** instancia del DTOMapper */
            val dogDTOMapper = DogDTOMapper()
            /** transforma el  DTOList en model domain por medio de DTOMapper */
            dogDTOMapper.fromDogDTOListToDogDomainList(dogDTOList)
        }
    }

    override suspend fun addDogToUser(dogId: Long): ApiResponseStatus<Any> = makeNetworkCall {
        val addDogToUserDTO = AddDogToUserDTO(dogId)
        val defaultResponse = apiService.addDogToUSer(addDogToUserDTO)
        if (!defaultResponse.isSuccess) {
            throw Exception(defaultResponse.message)
        }
    }


    /** Obtiene los perros que tiene el usuario guardado*/
    suspend fun getUserDogs(): ApiResponseStatus<List<Dog>> = makeNetworkCall {
        val dogListApiResponse = apiService.getUserDogs()
        val dogDTOList = dogListApiResponse.data.dogs
        val dogDTOMapper = DogDTOMapper()
        dogDTOMapper.fromDogDTOListToDogDomainList(dogDTOList)
    }

    /**metodo se utilza para reconosimiento de dogs*/
    override suspend fun getDogByMlId(mlDogID: String): ApiResponseStatus<Dog> = makeNetworkCall {
        val response = apiService.getDogByMLId(mlDogID)

        if (!response.isSuccess) {
            throw Exception(response.message)
        }
        val dogDTOMapper = DogDTOMapper()
        dogDTOMapper.fromDogDTOToDogDomain(response.data.dog)
    }


}