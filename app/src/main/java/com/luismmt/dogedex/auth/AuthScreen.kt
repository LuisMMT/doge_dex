package com.luismmt.dogedex.auth

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.luismmt.dogedex.api.ApiResponseStatus
import com.luismmt.dogedex.auth.AuthNavDestination.LoginScreenDestination
import com.luismmt.dogedex.auth.AuthNavDestination.SignUpScreenDestination
import com.luismmt.dogedex.composables.ErrorDialog
import com.luismmt.dogedex.composables.loadingWheel
import com.luismmt.dogedex.model.User


@Composable
fun AuthScreen(
    status: ApiResponseStatus<User>?,
    onLoginButtonClick: (email: String, password: String) -> Unit,
    onSignUpButtonClick: (email: String, password: String, confirmPassword: String) -> Unit,
    onErrorDialogDismiss: () -> Unit,
    authViewModel: AuthViewModel
) {
    /**Variable que guarda el estado de la navegacion*/
    val navController = rememberNavController()
    AuthNavHost(
        navController = navController,
        onLoginButtonClick = onLoginButtonClick,
        onSignUpButtonClick = onSignUpButtonClick,
        authViewModel = authViewModel,
        )

    if (status is ApiResponseStatus.LOADING) {
        loadingWheel()
    } else if (status is ApiResponseStatus.ERROR) {
        ErrorDialog(status.messageId, onErrorDialogDismiss)
    }


}


@Composable
fun AuthNavHost(
    navController: NavHostController,
    onLoginButtonClick: (email: String, password: String) -> Unit,
    onSignUpButtonClick: (email: String, password: String, confirmPassword: String) -> Unit,
    authViewModel: AuthViewModel,
) {
    /***navHost incluye las pantallas por la cuales se va a navegar*/
    NavHost(navController = navController, startDestination = LoginScreenDestination) {
        /***pantallas por la cuales se va a navegar**/
        composable(route = LoginScreenDestination) {
            LoginScreen(
                onLoginButtonClick = onLoginButtonClick,
                onRegisterButtonClick = { navController.navigate(route = SignUpScreenDestination) },
                authViewModel = authViewModel
            )
        }
        composable(route = SignUpScreenDestination) {
            /**** navigateUp indica que regrese al destino anterior */
            SignupScreen(
                onSignUpButtonClick = onSignUpButtonClick,
                onNavigationIconClick = { navController.navigateUp() },
                authViewModel = authViewModel
            )
        }
    }
}
