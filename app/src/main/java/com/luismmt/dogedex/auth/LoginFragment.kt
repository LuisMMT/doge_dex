package com.luismmt.dogedex.auth

import android.content.Context
import android.os.Bundle
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.luismmt.dogedex.R
import com.luismmt.dogedex.databinding.FragmentLoginBinding
import com.luismmt.dogedex.databinding.FragmentSingUpBinding
import com.luismmt.dogedex.isValidEmail
import kotlin.math.log


class LoginFragment : Fragment() {

    /**permite que del login Activity navegue al singUpFragment por medio de interfaz */
    interface LoginFragmentActions {
        /**
         * metodo que va a tener evento de navegar a la siguiente pantalla
         */
        fun onRegisterButtonClick()

        /** */
        fun onLoginFieldsValidation(emial:String,password:String)

    }

    private lateinit var loginFragmentActions: LoginFragmentActions

    /** binding global de login*/
    private lateinit var binding: FragmentLoginBinding

    /**
     * Permite que cuando este fragmene se une al activity le
     * pasa el contexto al fragment el cual se utilza
     *
     */
    override fun onAttach(context: Context) {
        super.onAttach(context)
        loginFragmentActions = try {
            context as LoginFragmentActions
        } catch (e: java.lang.ClassCastException) {
            throw java.lang.ClassCastException("$context must implement LoginFragmentActions")
        }
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        /**
         * inflar layout por medio de binding
         */
         binding = FragmentLoginBinding.inflate(layoutInflater)

        /**
         * crear evendo al presionar boton register por medio de la interfaz LoginFragmentActions
         */
        binding.loginRegisterButton.setOnClickListener {
            loginFragmentActions.onRegisterButtonClick()
        }

        /** crear evento click para login */
        binding.loginButton.setOnClickListener {
            validateFields()
        }
        return binding.root
    }

    private fun validateFields() {
        binding.emailInput.error =""
        binding.passwordInput.error =""

        val email = binding.emailEdit.text.toString()
        if (!isValidEmail(email)) {
            binding.emailInput.error = getString(R.string.email_is_not_valid)
            return
        }

        val password = binding.passwordEdit.text.toString()
        if (password.isEmpty()) {
            binding.passwordInput.error = getString(R.string.password_must_not_be_empty)
            return
        }
        loginFragmentActions.onLoginFieldsValidation(email,password)
    }

}