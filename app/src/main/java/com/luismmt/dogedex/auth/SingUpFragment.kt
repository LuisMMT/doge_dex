package com.luismmt.dogedex.auth

import android.content.Context
import android.os.Bundle
import android.provider.ContactsContract.CommonDataKinds.Email
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.luismmt.dogedex.R
import com.luismmt.dogedex.databinding.FragmentSingUpBinding
import com.luismmt.dogedex.isValidEmail
import kotlin.math.sin

class SingUpFragment : Fragment() {

    /**
     *
     */
    interface SingUpFragmentActions{
        fun onSingUpFieldsValidated(email: String,password:String,ConfirmsPassword:String)
    }
    private lateinit var singUpFragmentActions: SingUpFragmentActions

    override fun onAttach(context: Context) {
        super.onAttach(context)
        singUpFragmentActions = try {
            context as SingUpFragmentActions
        }catch (e:java.lang.ClassCastException){
            throw java.lang.ClassCastException("$context must Implement SingUpFragment")
        }
    }

    private lateinit var binding: FragmentSingUpBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSingUpBinding.inflate(layoutInflater)
        setupSingUpBotton()
        return binding.root
    }

    private fun setupSingUpBotton() {
        binding.signUpButton.setOnClickListener {
            validateFields()
        }

    }

    private fun validateFields() {
        binding.emailInput.error =""
        binding.passwordInput.error =""
        binding.confirmPasswordInput.error = ""
        val email = binding.emailEdit.text.toString()
        if (!isValidEmail(email)) {
            binding.emailInput.error = getString(R.string.email_is_not_valid)
            return
        }

        val password = binding.passwordEdit.text.toString()
        if (password.isEmpty()) {
            binding.passwordInput.error = getString(R.string.password_must_not_be_empty)
            return
        }

        val confirmsPassword = binding.confirmPasswordEdit.text.toString()
        if (confirmsPassword.isEmpty()) {
            binding.confirmPasswordInput.error = getString(R.string.password_must_not_be_empty)
            return
        }

        if (password != confirmsPassword) {
            binding.passwordInput.error = getString(R.string.password_do_no_match)
            return
        }

       singUpFragmentActions.onSingUpFieldsValidated(email,password,confirmsPassword)
    }


}