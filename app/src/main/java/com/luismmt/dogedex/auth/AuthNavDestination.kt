package com.luismmt.dogedex.auth


object AuthNavDestination{
    const val LoginScreenDestination = "login_screen"
    const val SignUpScreenDestination ="sign_up_screen"
}