package com.luismmt.dogedex.auth


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.findNavController
import com.luismmt.dogedex.main.MainActivity
import com.luismmt.dogedex.R
import com.luismmt.dogedex.api.ApiResponseStatus


import com.luismmt.dogedex.databinding.ActivityLoginBinding
import com.luismmt.dogedex.dogDetail.ui.theme.DogeDexTheme
import com.luismmt.dogedex.model.User
import dagger.hilt.android.AndroidEntryPoint

/**
 * LoginFragment.LoginFragmentActions
 */
@AndroidEntryPoint
class LoginActivity : ComponentActivity() {

    private val viewModel: AuthViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val user = viewModel.user
            val userValue = user.value
            if (userValue != null) {
                User.setLoggedInUser(this, userValue)
                goToMainActivity()
            }


            val status = viewModel.status
            DogeDexTheme {
                AuthScreen(
                    status = status.value,
                    onLoginButtonClick = { email, password -> viewModel.login(email, password) },
                    onSignUpButtonClick = { email, password, confirmPassword ->
                        viewModel.singUp(
                            email = email,
                            password = password,
                            confirmPassword = confirmPassword
                        )
                    },
                    onErrorDialogDismiss = ::resetApiResponseStatus,
                    authViewModel = viewModel
                )
            }
        }


    }


    private fun resetApiResponseStatus() {
        viewModel.resetApiResponseStatus()
    }

    private fun goToMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }


}