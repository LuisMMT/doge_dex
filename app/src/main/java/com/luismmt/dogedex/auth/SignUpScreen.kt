package com.luismmt.dogedex.auth

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.luismmt.dogedex.R
import com.luismmt.dogedex.composables.AuthField
import com.luismmt.dogedex.composables.BackNavigationIcon


@Composable
fun SignupScreen(
    onSignUpButtonClick: (email: String, password: String, confirmPassword: String) -> Unit,
    onNavigationIconClick: () -> Unit,
    authViewModel: AuthViewModel
) {
    Scaffold(topBar = { SignUpScreenToolbar(onNavigationIconClick) }) {
        content(
            resetFieldError = { authViewModel.resetError() },
            onSignUpButtonClick = onSignUpButtonClick,
            authViewModel = authViewModel
        )

    }

}


@Composable
fun SignUpScreenToolbar(onNavigationIconClick: () -> Unit) {
    TopAppBar(title = { Text(text = stringResource(id = R.string.signUp)) },
        backgroundColor = Color.Red,
        contentColor = Color.White,
        navigationIcon = {
            BackNavigationIcon { onNavigationIconClick() }
        })
}


@Composable
private fun content(
    resetFieldError: () -> Unit,
    onSignUpButtonClick: (email: String, password: String, confirmPassword: String) -> Unit,
    authViewModel: AuthViewModel
) {
    // email , password, confirm password
    val email = remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }
    val confirmPassword = remember { mutableStateOf("") }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 32.dp, start = 16.dp, end = 16.dp, bottom = 16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        AuthField(
            label = stringResource(id = R.string.email),
            modifier = Modifier.fillMaxWidth(),
            email = email.value,
            onTextChanged = {
                email.value = it
                resetFieldError()
            },
            errorMessageId = authViewModel.emailError.value
        )
        AuthField(
            label = stringResource(id = R.string.password),
            modifier = Modifier.fillMaxWidth(),
            email = password,
            visualTransformation = PasswordVisualTransformation(),
            onTextChanged = {
                password = it
                resetFieldError()
            },
            errorMessageId = authViewModel.passwordError.value
        )

        AuthField(
            label = stringResource(id = R.string.confirm_password),
            modifier = Modifier.fillMaxWidth(),
            email = confirmPassword.value,
            visualTransformation = PasswordVisualTransformation(),
            onTextChanged = {
                confirmPassword.value = it
                resetFieldError()
            },
            errorMessageId = authViewModel.confirmPasswordError.value
        )

        Button(modifier = Modifier
            .fillMaxWidth()
            .padding(top = 16.dp),
            onClick = { onSignUpButtonClick(email.value, password, confirmPassword.value) }) {
            Text(
                stringResource(id = R.string.signUp),
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.Medium
            )

        }
    }


}

