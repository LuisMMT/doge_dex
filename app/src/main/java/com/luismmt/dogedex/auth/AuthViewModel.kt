package com.luismmt.dogedex.auth

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.luismmt.dogedex.R
import com.luismmt.dogedex.api.ApiResponseStatus
import com.luismmt.dogedex.model.User
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor(
    private val authRepository: AuthTask
) : ViewModel() {

    var user = mutableStateOf<User?>(null)
        private set

    var emailError = mutableStateOf<Int?>(null)
        private set

    var passwordError = mutableStateOf<Int?>(null)
        private set

    var confirmPasswordError = mutableStateOf<Int?>(null)
        private set

    /**  variable privada que captura respuesta del api*/
    var status = mutableStateOf<ApiResponseStatus<User>?>(null)
        private set    // para manterner el encapsulamiento se utiliza set


    fun singUp(email: String, password: String, confirmPassword: String) {

        when {
            email.isEmpty() -> {
                emailError.value = R.string.email_is_not_valid
            }
            password.isEmpty() -> {
                passwordError.value = R.string.password_must_not_be_empty
            }
            confirmPassword.isEmpty() -> {
                confirmPasswordError.value = R.string.password_must_not_be_empty
            }
            password != confirmPassword -> {
                passwordError.value = R.string.password_do_no_match
                confirmPasswordError.value = R.string.password_do_no_match
            }
            else -> {
                viewModelScope.launch {
                    status.value = ApiResponseStatus.LOADING()
                    handleResponseStatus(authRepository.singUp(email, password, confirmPassword))
                }
            }
        }
    }

    fun resetError() {
        emailError.value = null
        passwordError.value = null
        confirmPasswordError.value = null
    }

    fun login(email: String, password: String) {
        when {
            email.isEmpty() -> emailError.value = R.string.email_is_not_valid
            password.isEmpty() -> passwordError.value = R.string.password_must_not_be_empty
            else -> {
                viewModelScope.launch {
                    status.value = ApiResponseStatus.LOADING()
                    handleResponseStatus(authRepository.Login(email, password))
                }
            }
        }

    }

    private fun handleResponseStatus(apiResponseStatus: ApiResponseStatus<User>) {
        if (apiResponseStatus is ApiResponseStatus.SUCCESS) {
            user.value = apiResponseStatus.data!!
        }
        status.value = apiResponseStatus
    }

    fun resetApiResponseStatus() {
        status.value = null
    }


}