package com.luismmt.dogedex.auth

import com.luismmt.dogedex.model.User
import com.luismmt.dogedex.api.ApiResponseStatus
import com.luismmt.dogedex.api.ApiService
import com.luismmt.dogedex.api.dto.LogInDTO
import com.luismmt.dogedex.api.dto.SignUpDTO
import com.luismmt.dogedex.api.dto.UserDTOMapper
import com.luismmt.dogedex.api.makeNetworkCall
import javax.inject.Inject


interface AuthTask {
    suspend fun Login(
        email: String,
        password: String,
    ): ApiResponseStatus<User>

    suspend fun singUp(
        email: String, password: String, confirmPassword: String
    ): ApiResponseStatus<User>
}

class AuthRepository @Inject constructor(
    private val apiService: ApiService,
) : AuthTask {

    override suspend fun Login(
        email: String,
        password: String,
    ): ApiResponseStatus<User> = makeNetworkCall {
        val logInDTO = LogInDTO(email, password)
        val logInResponse = apiService.signIn(logInDTO)

        if (!logInResponse.isSuccess) {
            throw Exception(logInResponse.message)
        }

        val userDTO = logInResponse.data.user
        val userDTOMapper = UserDTOMapper()
        userDTOMapper.fromUserDTOToUserDomain(userDTO)
    }


    override suspend fun singUp(
        email: String, password: String, confirmPassword: String
    ): ApiResponseStatus<User> = makeNetworkCall {
        val signUPDTO = SignUpDTO(email, password, confirmPassword)
        val signUpResponse = apiService.signUp(signUPDTO)

        if (!signUpResponse.isSuccess) {
            throw Exception(signUpResponse.message)
        }

        val userDTO = signUpResponse.data.user
        val userDTOMapper = UserDTOMapper()
        userDTOMapper.fromUserDTOToUserDomain(userDTO)
    }
}