package com.luismmt.dogedex

/**
 * constantes del proyecto
 */
const val BASE_URL = "https://todogs.herokuapp.com/api/v1/"
const val GET_ALL_DOGS = "dogs"
const val SING_UP_URL = "sign_up"
const val SING_IN_URL = "sign_in"
const val ADD_DOG_TO_USER = "add_dog_to_user"
const val GET_USERS_DOGS_URL = "get_user_dogs"
const val GET_DOG_BY_ML_ID = "find_dog_by_ml_id"


//Constantes para reconocimiento
const val MAX_RECOGNITION_DOG_RESULTS = 5
const val MODEL_PATH = "model.tflite"
const val LABEL_TXT = "labels.txt"