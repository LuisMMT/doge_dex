package com.luismmt.dogedex.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class Dog(
    val id: Long,
    val type: String,
    val height_female: String,
    val height_male: String,
    val image_url: String,
    val index: Int,
    val life_expectancy: String,
    val name_en: String,
    val name: String,
    val temperament: String,
    val temperament_en: String,
    val weight_female: String,
    val weightMale: String,
    val created_at: String,
    val updated_at: String,
    val ml_id: String,
    var inCollection :Boolean = true
) : Parcelable, Comparable<Dog> {
    override fun compareTo(other: Dog): Int {
        return if (this.index>other.index){
            1
        }else{
            -1
        }
    }

}
