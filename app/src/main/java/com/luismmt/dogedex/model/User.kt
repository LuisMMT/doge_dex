package com.luismmt.dogedex.model

import android.app.Activity
import android.content.Context


class User(
    val id: Long,
    val email: String,
    val authenticationToken: String
) {

    /** permite guardar la sesion*/

    companion object {
        private const val AUTH_PREF = "auth_pref"
        private const val ID = "id"
        private const val EMAIL = "email"
        private const val AUTH_TOKEN = "auth_token"

        /** Guarda la session*/
        fun setLoggedInUser(activity: Activity, user: User) {
            activity.getSharedPreferences(AUTH_PREF, Context.MODE_PRIVATE).also {
                it.edit()
                    .putLong(ID, user.id)
                    .putString(EMAIL, user.email)
                    .putString(AUTH_TOKEN, user.authenticationToken)
                    .apply()

            }
        }

        /** obtiene la session*/
        fun getLoggedInUser(activity: Activity): User? {
            val prefUser =
                activity.getSharedPreferences(AUTH_PREF, Context.MODE_PRIVATE) ?: return null


            val userId = prefUser.getLong(ID, 0)
            if (userId == 0L) {
                return null
            }
            return User(
                userId, prefUser.getString(EMAIL, "") ?: "",
                prefUser.getString(AUTH_TOKEN, "") ?: ""
            )
        }

        /** borre preferencia del usuario*/
        fun logout(activity: Activity) {
            activity.getSharedPreferences(AUTH_PREF, Context.MODE_PRIVATE).also {
                it.edit().clear().apply()
            }
        }
    }

}


