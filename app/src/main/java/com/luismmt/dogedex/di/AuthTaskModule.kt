package com.luismmt.dogedex.di

import com.luismmt.dogedex.auth.AuthRepository
import com.luismmt.dogedex.auth.AuthTask
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class AuthTaskModule {

    @Binds
    abstract fun bindDogTask(
        authRepository:AuthRepository
    ):AuthTask


}