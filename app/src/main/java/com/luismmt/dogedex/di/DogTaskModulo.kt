package com.luismmt.dogedex.di

import com.luismmt.dogedex.dogList.DogListRepository
import com.luismmt.dogedex.dogList.DogTask
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module //
@InstallIn(SingletonComponent::class) // de que parte quiero que sea visible el modulo (Scope)
abstract class DogTaskModulo {


    /***
     * Metodo que hace la union entre interfaz y la implementacion que se inyecta**/
    @Binds
    abstract fun bindDogTasks(dogListRepository: DogListRepository): DogTask


}