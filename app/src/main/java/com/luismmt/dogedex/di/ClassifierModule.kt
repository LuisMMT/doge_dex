package com.luismmt.dogedex.di

import com.luismmt.dogedex.machinelearning.ClassifierRepository
import com.luismmt.dogedex.machinelearning.ClassifierTask
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent


@Module
@InstallIn(SingletonComponent::class)
abstract class ClassifierModule {

    @Binds
    abstract fun bindClassifierTasks(classifierRepository: ClassifierRepository): ClassifierTask
}