package com.luismmt.dogedex.dogDetail

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.luismmt.dogedex.R
import com.luismmt.dogedex.api.ApiResponseStatus
import com.luismmt.dogedex.dogList.DogListRepository
import com.luismmt.dogedex.dogList.DogTask
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DogDetailViewModel @Inject constructor(
    private val dogListRepository: DogTask
) : ViewModel() {


    /**  variable privada que captura respuesta del api*/
    var status = mutableStateOf<ApiResponseStatus<Any>?>(null)
        private set    // para manterner el encapsulamiento se utiliza set


    /**Agrega dog al usuario*/
    fun addDogToUser(dogId: Long) {
        viewModelScope.launch {
            status.value = ApiResponseStatus.LOADING()
            handleAddDogToUserResponseStatus(dogListRepository.addDogToUser(dogId))
        }
    }

    private fun handleAddDogToUserResponseStatus(apiResponseStatus: ApiResponseStatus<Any>) {
        status.value = apiResponseStatus

    }

    fun resetApiResponseStatus() {
        status.value = null
    }


}