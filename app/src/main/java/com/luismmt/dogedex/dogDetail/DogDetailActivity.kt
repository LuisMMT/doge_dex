package com.luismmt.dogedex.dogDetail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import coil.load
import com.luismmt.dogedex.model.Dog
import com.luismmt.dogedex.R
import com.luismmt.dogedex.api.ApiResponseStatus
import com.luismmt.dogedex.databinding.ActivityDogDetailBinding

class DogDetailActivity : AppCompatActivity() {


    companion object {
        const val DOG_KEY = "dog"
        const val IS_RECONIGTION_KEY = "is_recognition"
    }

    private val viewModel: DogDetailViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityDogDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //recibe el objeto para mostrar
        val dog = intent?.extras?.getParcelable<Dog>(DOG_KEY)
        val isReconigtion = intent?.extras?.getBoolean(IS_RECONIGTION_KEY, false) ?: false

        // se valida si no existe un dog muestra el sig mensaje.
        if (dog == null) {
            Toast.makeText(this, R.string.error_showing_dog_not_found, Toast.LENGTH_LONG).show()
            finish()
            return
        }
        binding.dogIndex.text = getString(R.string.dog_index_format, dog.index)
        binding.lifeExpectancy.text =
            getString(R.string.dog_life_expectancy_format, dog.life_expectancy)

        binding.dog = dog
        binding.dogImage.load(dog.image_url)


        // se comenta xq cambio la logica para dar soporte a jetPack composer
        /** Observa cambio en cada estado de la llamada para verificar el progresBar */
      /*  viewModel.status.observe(this) { status ->
            *//** Evalua cada estado retornado de la respuesta de viewModel _status, muesta o no el progressBar *//*
            when (status) {
                is ApiResponseStatus.ERROR -> {
                    binding.loadingWheel.visibility = View.GONE
                    Toast.makeText(this, status.messageId, Toast.LENGTH_LONG).show()
                }

                is ApiResponseStatus.LOADING -> binding.loadingWheel.visibility = View.VISIBLE
                is ApiResponseStatus.SUCCESS -> {
                    binding.loadingWheel.visibility = View.GONE
                    finish()
                }
            }
        }*/
        binding.closeButton.setOnClickListener {
            if (isReconigtion) {
                viewModel.addDogToUser(dog.id)
            } else {
                finish()
            }

        }

    }
}