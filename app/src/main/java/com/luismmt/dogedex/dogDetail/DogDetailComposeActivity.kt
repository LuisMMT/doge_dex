package com.luismmt.dogedex.dogDetail

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.lifecycle.viewmodel.compose.viewModel
import com.luismmt.dogedex.R
import com.luismmt.dogedex.api.ApiResponseStatus
import com.luismmt.dogedex.dogDetail.ui.theme.DogeDexTheme
import com.luismmt.dogedex.model.Dog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DogDetailComposeActivity : ComponentActivity() {

    companion object {
        const val DOG_KEY = "dog"
        const val IS_RECONIGTION_KEY = "is_recognition"
    }

    private val viewModel: DogDetailViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //recibe el objeto para mostrar
        val dog = intent?.extras?.getParcelable<Dog>(DOG_KEY)
        val isRecognition = intent?.extras?.getBoolean(IS_RECONIGTION_KEY, false) ?: false

        // se valida si no existe un dog muestra el sig mensaje.
        if (dog == null) {
            Toast.makeText(this, R.string.error_showing_dog_not_found, Toast.LENGTH_LONG).show()
            finish()
            return
        }

        setContent {
            /*** para jetpack compose no se utiliza MutableLiveData sino mutableStateOf**/
            val status = viewModel.status
            if (status.value is ApiResponseStatus.SUCCESS) {
                finish()
            } else {
                DogeDexTheme {
                    DogDetailScreen(
                        dog = dog,
                        status = status.value,
                        onButtonClicked = { onButtonClicked(dog.id, isRecognition) },
                        onErrorDialogDismiss = ::resetApiResponseStatus
                    )
                }
            }

        }
    }

    private fun resetApiResponseStatus() {
        viewModel.resetApiResponseStatus()
    }

    private fun onButtonClicked(dogId: Long, isRecognition: Boolean) {
        if (isRecognition) {
            viewModel.addDogToUser(dogId)
        }
    }
}

