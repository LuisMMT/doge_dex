package com.luismmt.dogedex.api.responses

import com.luismmt.dogedex.api.dto.DogDTO

class DogResponse(val dog:DogDTO)