package com.luismmt.dogedex.api.dto

import com.squareup.moshi.Json
import retrofit2.http.Field

class UserDTO(
    val id: Long,
    val email: String,
    @field:Json(name = "authentication_token") val authenticationToken: String
) {

}