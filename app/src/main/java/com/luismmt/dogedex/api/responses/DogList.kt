package com.luismmt.dogedex.api.responses

import com.luismmt.dogedex.api.dto.DogDTO

class DogList(val dogs: List<DogDTO>)