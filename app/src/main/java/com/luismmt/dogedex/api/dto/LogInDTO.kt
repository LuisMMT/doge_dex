package com.luismmt.dogedex.api.dto

class LogInDTO(
    val email: String,
    val password: String,
)