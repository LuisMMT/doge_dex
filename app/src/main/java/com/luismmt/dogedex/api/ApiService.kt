package com.luismmt.dogedex.api

import com.luismmt.dogedex.*
import com.luismmt.dogedex.api.dto.AddDogToUserDTO
import com.luismmt.dogedex.api.dto.LogInDTO
import com.luismmt.dogedex.api.dto.SignUpDTO
import com.luismmt.dogedex.api.responses.DogListApiResponse
import com.luismmt.dogedex.api.responses.AuthApiResponse
import com.luismmt.dogedex.api.responses.DefaultResponse
import com.luismmt.dogedex.api.responses.DogApiResponse
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*


/** interface que hace llamada a las url */
interface ApiService {
    /**  metodo para obtener lista de perros */
    @GET(GET_ALL_DOGS)
    suspend fun getAllDogs(): DogListApiResponse

    @POST(SING_UP_URL)
    suspend fun signUp(@Body signUpDTO: SignUpDTO): AuthApiResponse

    @POST(SING_IN_URL)
    suspend fun signIn(@Body LogInDTO: LogInDTO): AuthApiResponse

    @Headers("${ApiServiceInterceptor.NEEDS_AUTH_HEADER_KEY}:true")
    @POST(ADD_DOG_TO_USER)
    suspend fun addDogToUSer(@Body addDogToUserDTO: AddDogToUserDTO): DefaultResponse

    @Headers("${ApiServiceInterceptor.NEEDS_AUTH_HEADER_KEY}:true")
    @GET(GET_USERS_DOGS_URL)
    suspend fun getUserDogs(): DogListApiResponse

    @GET(GET_DOG_BY_ML_ID)
    suspend fun getDogByMLId(@Query("ml_id") mlId: String): DogApiResponse

}
