package com.luismmt.dogedex.api

import com.luismmt.dogedex.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.net.UnknownHostException

private const val UNAUTHORIZED_ERROR_CODE = 401

/**
 * realiza llamada al api
 */
suspend fun <T> makeNetworkCall(
    call: suspend () -> T
): ApiResponseStatus<T> {
    return withContext(Dispatchers.IO) {
        try {
            ApiResponseStatus.SUCCESS(call())
        } catch (e: UnknownHostException) {
            ApiResponseStatus.ERROR(R.string.unknow_exception_error)
        } catch (e: HttpException) {
            val erroMessage = if (e.code() == UNAUTHORIZED_ERROR_CODE) {
                R.string.wrong_user_or_password
            } else {
                R.string.unKnow_error
            }
            ApiResponseStatus.ERROR(erroMessage)
        } catch (e: Exception) {
            val errorMessage = when (e.message) {
                "sign_up_error" -> R.string.error_sign_up
                "sign_in_error" -> R.string.error_sign_in
                "user_already_exist" -> R.string.user_already_exist
                "Password needs at least 8 characters" -> R.string.password_needs_at_least_8_character
                "error_adding_dog" -> R.string.error_adding_dog
                else -> R.string.unKnow_error
            }
            ApiResponseStatus.ERROR(errorMessage)
        }
    }
}