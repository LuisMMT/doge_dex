package com.luismmt.dogedex.api.dto

import com.luismmt.dogedex.model.Dog

class DogDTOMapper {
    fun fromDogDTOToDogDomain(dogDTO: DogDTO): Dog {
        return Dog(
            dogDTO.id,
            dogDTO.type,
            dogDTO.height_female,
            dogDTO.height_male,
            dogDTO.image_url,
            dogDTO.index,
            dogDTO.life_expectancy,
            dogDTO.name_en,
            dogDTO.name,
            dogDTO.temperament,
            dogDTO.temperament_en,
            dogDTO.weight_female,
            dogDTO.weightMale,
            dogDTO.created_at,
            dogDTO.updated_at,
            dogDTO.ml_id,
        )
    }

    fun fromDogDTOListToDogDomainList(dogDTOList: List<DogDTO>): List<Dog> {
        return dogDTOList.map { fromDogDTOToDogDomain(it) }
    }
}