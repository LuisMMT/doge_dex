package com.luismmt.dogedex.api

import okhttp3.Interceptor
import okhttp3.Response
/** se crea interceptores para el api**/
object ApiServiceInterceptor:Interceptor {

    const val NEEDS_AUTH_HEADER_KEY = "needs_authentication"

    //verifica si existe session token
    private var sessionToken:String?=null
    fun setSessionToken(sessionToken:String){
        this.sessionToken = sessionToken
    }

    /** agrega header al los recursos q tenga la sontaxis @Header()*/
    override fun intercept(chain: Interceptor.Chain): Response {
       val request = chain.request()
        val requestBuilder = request.newBuilder()
        if (request.header(NEEDS_AUTH_HEADER_KEY)!=null){
            //necesita credencial
            if (sessionToken == null){
                throw java.lang.RuntimeException("Need to be authenticated to perform")
            }else{
                requestBuilder.addHeader("AUTH-TOKEN",sessionToken!!)
            }
        }
        return chain.proceed(requestBuilder.build())
    }
}