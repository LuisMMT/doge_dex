package com.luismmt.dogedex.api.responses

import com.luismmt.dogedex.api.dto.UserDTO

class UserResponse(val user: UserDTO)