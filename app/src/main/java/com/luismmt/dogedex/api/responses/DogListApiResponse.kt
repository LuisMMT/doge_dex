package com.luismmt.dogedex.api.responses

import com.squareup.moshi.Json

/**
 * Modelo de respuesta
 */
class DogListApiResponse(
    val message: String,
    @field:Json(name = "is_success") val isSuccess: Boolean,
    val data: DogList
)