package com.luismmt.dogedex.api

/**
 * Maneja la respuesta con Genericos
 */
sealed class ApiResponseStatus<T>() {
    /** retorna respuesta del api cuando es respuesta ok */
    class SUCCESS<T>(val data: T) : ApiResponseStatus<T>()

    /** indica que esta cargando una llamada al api */
    class LOADING<T>() : ApiResponseStatus<T>()

    /** retorna error en la respuesta del api */
    class ERROR<T>(val messageId: Int) : ApiResponseStatus<T>()
}