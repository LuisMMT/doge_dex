@file:OptIn(ExperimentalMaterialApi::class)

package com.luismmt.dogedex.main

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.compose.material.ExperimentalMaterialApi
import androidx.core.content.ContextCompat
import com.luismmt.dogedex.LABEL_TXT
import com.luismmt.dogedex.MODEL_PATH
import com.luismmt.dogedex.R
import com.luismmt.dogedex.WholeImageActivity
import com.luismmt.dogedex.WholeImageActivity.Companion.PHOTO_URI_KEY
import com.luismmt.dogedex.api.ApiResponseStatus
import com.luismmt.dogedex.api.ApiServiceInterceptor
import com.luismmt.dogedex.auth.LoginActivity
import com.luismmt.dogedex.databinding.ActivityMainBinding
import com.luismmt.dogedex.dogDetail.DogDetailComposeActivity
import com.luismmt.dogedex.dogDetail.DogDetailComposeActivity.Companion.DOG_KEY
import com.luismmt.dogedex.dogDetail.DogDetailComposeActivity.Companion.IS_RECONIGTION_KEY
import com.luismmt.dogedex.dogList.DogListActivity
import com.luismmt.dogedex.machinelearning.Classifier
import com.luismmt.dogedex.machinelearning.DogRecognition
import com.luismmt.dogedex.model.Dog
import com.luismmt.dogedex.model.User
import com.luismmt.dogedex.settings.SettingsActivity
import dagger.hilt.android.AndroidEntryPoint
import org.tensorflow.lite.support.common.FileUtil
import java.io.File
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {


    private lateinit var cameraExecutor: ExecutorService
    private lateinit var imageCapture: ImageCapture
    lateinit var binding: ActivityMainBinding
    private var isCameraReady = false
    private val viewModel: MainViewModel by viewModels()


    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (isGranted) {
            // Permission is granted. Continue the action or workflow in your
            // app.

            setupCamera()
        } else {
            Toast.makeText(this, "", Toast.LENGTH_LONG).show()
            // Explain to the user that the feature is unavailable because the
            // feature requires a permission that the user has denied. At the
            // same time, respect the user's decision. Don't link to system
            // settings in an effort to convince the user to change their
            // decision.
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        /** valida si el usuario tiene una session**/
        val user = User.getLoggedInUser(this)
        if (user == null) {
            openLoginActivity()
            return
        } else {
            ApiServiceInterceptor.setSessionToken(user.authenticationToken)
        }

        /** redirige a boton logout*/
        binding.settings.setOnClickListener {
            openSettingsActicity()
        }
        binding.listFab.setOnClickListener {
            openDogListActivity()
        }


        viewModel.status.observe(this) { status ->
            /** Evalua cada estado retornado de la respuesta de viewModel _status, muesta o no el progressBar */
            when (status) {
                is ApiResponseStatus.ERROR -> {
                    binding.loadingWheel.visibility = View.GONE
                    Toast.makeText(this, status.messageId, Toast.LENGTH_LONG).show()
                }

                is ApiResponseStatus.LOADING -> binding.loadingWheel.visibility = View.VISIBLE
                is ApiResponseStatus.SUCCESS -> binding.loadingWheel.visibility = View.GONE
            }
        }

        viewModel.dog.observe(this) { dog ->
            if (dog != null) {
                openDogDetailActivity(dog)
            }
        }

        viewModel.dogRecognition.observe(this) {
            enableTakePhotoButton(it)
        }


        requestCameraPermission()

    }

    private fun openDogDetailActivity(dog: Dog) {
        val intent = Intent(this, DogDetailComposeActivity::class.java)
        intent.putExtra(DOG_KEY, dog)
        intent.putExtra(
            IS_RECONIGTION_KEY, true
        )
        startActivity(intent)
    }


    override fun onDestroy() {
        super.onDestroy()
        if (::cameraExecutor.isInitialized) {
            cameraExecutor.shutdown()
        }
    }

    private fun requestCameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            when {
                ContextCompat.checkSelfPermission(
                    this, android.Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_GRANTED -> {
                    // You can use the API that requires the permission.
                    setupCamera()
                }
                shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA) -> {
                    // In an educational UI, explain to the user why your app requires this
                    // permission for a specific feature to behave as expected, and what
                    // features are disabled if it's declined. In this UI, include a
                    // "cancel" or "no thanks" button that lets the user continue
                    // using your app without granting the permission.
                    AlertDialog.Builder(this).setTitle("Aceptame").setMessage("Acepta la camera ")
                        .setPositiveButton(android.R.string.ok) { _, _ ->
                            requestPermissionLauncher.launch(android.Manifest.permission.CAMERA)
                        }.setNegativeButton(android.R.string.cancel) { _, _ ->
                        }.show()

                }
                else -> {
                    // You can directly ask for the permission.
                    // The registered ActivityResultCallback gets the result of this request.
                    requestPermissionLauncher.launch(
                        android.Manifest.permission.CAMERA
                    )
                }
            }
        } else {
            setupCamera()
        }
    }

    /***inicia la camera */
    private fun startCamera() {
        /** es un singleton que permite bindear con el ciclo de vidad del activity , solo tiene un proceos a la vez*/
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)


        cameraProviderFuture.addListener({

            val cameraProvider = cameraProviderFuture.get()
            //se crea un preview
            val preview = Preview.Builder().build()
            // se agrega una superfice q es conde se va a ver
            preview.setSurfaceProvider(binding.cameraPreview.surfaceProvider)

            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            val imageAnalysis = ImageAnalysis.Builder()
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .build()
            imageAnalysis.setAnalyzer(cameraExecutor) { imageProxy ->
                viewModel.recognizeImage(imageProxy)

            }


            //se liga con el life cicle
            cameraProvider.bindToLifecycle(
                this,
                cameraSelector,
                preview,
                imageCapture,
                imageAnalysis
            )
        }, ContextCompat.getMainExecutor(this))
    }

    private fun enableTakePhotoButton(dogReconigtion: DogRecognition) {

        if (dogReconigtion.confidence > 50.0) {
            binding.takePhotoFab.alpha = 1f
            binding.takePhotoFab.setOnClickListener {
                viewModel.getDogByMlId(dogReconigtion.id)
            }
        } else {
            binding.takePhotoFab.alpha = 0.2f
            binding.takePhotoFab.setOnClickListener(null)
        }

    }

    private fun setupCamera() {
        // se agrega a un runnable para esperar  a q se inicialise.
        binding.cameraPreview.post {
            imageCapture = ImageCapture.Builder()
                .setTargetRotation(binding.cameraPreview.display.rotation)
                .build()
            // se crea un hilo para q se ejecute la camara en un nuevo hilo
            cameraExecutor = Executors.newSingleThreadExecutor()
            startCamera()
            isCameraReady = true
        }
    }

    private fun takePhoto() {
        //file donde se va a guardar el archivo
        val outputFileOptions = ImageCapture.OutputFileOptions.Builder(getOutputPhotoFile()).build()

        imageCapture.takePicture(
            outputFileOptions,
            cameraExecutor,
            object : ImageCapture.OnImageSavedCallback {
                override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {

                }

                override fun onError(exception: ImageCaptureException) {
                    Toast.makeText(
                        this@MainActivity, exception.message,
                        Toast.LENGTH_LONG
                    ).show()
                }

            })

    }

    private fun openWholePhotoImageActivity(photoUri: String) {
        val intent = Intent(this, WholeImageActivity::class.java)
        intent.putExtra(PHOTO_URI_KEY, photoUri)
        startActivity(intent)

    }


    private fun getOutputPhotoFile(): File {
        //Toma un directorio del arreglo de directorios de android
        //Toma el primero que no sea null y de hijo pone el nombre del app
        val mediaDir = externalMediaDirs.firstOrNull()?.let {
            File(it, resources.getString(R.string.app_name) + ".jpg").apply { mkdir() }
        }
        return if (mediaDir != null && mediaDir.exists()) {
            mediaDir
        } else {
            filesDir
        }
    }

    private fun openDogListActivity() {
        startActivity(Intent(this, DogListActivity::class.java))
    }

    private fun openSettingsActicity() {
        startActivity(Intent(this, SettingsActivity::class.java))
    }

    private fun openLoginActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }


}