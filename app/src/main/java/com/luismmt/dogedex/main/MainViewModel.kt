package com.luismmt.dogedex.main

import androidx.camera.core.ImageProxy
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.luismmt.dogedex.api.ApiResponseStatus
import com.luismmt.dogedex.dogList.DogListRepository
import com.luismmt.dogedex.dogList.DogTask
import com.luismmt.dogedex.machinelearning.Classifier
import com.luismmt.dogedex.machinelearning.ClassifierRepository
import com.luismmt.dogedex.machinelearning.ClassifierTask
import com.luismmt.dogedex.machinelearning.DogRecognition
import com.luismmt.dogedex.model.Dog
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.nio.MappedByteBuffer
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val dogRepository: DogTask,
    private val classifierRepository: ClassifierTask,
) : ViewModel() {


    private val _dog = MutableLiveData<Dog>()
    val dog: LiveData<Dog>
        get() = _dog

    private val _status = MutableLiveData<ApiResponseStatus<Dog>>()
    val status: LiveData<ApiResponseStatus<Dog>>
        get() = _status

    private val _dogRecognition = MutableLiveData<DogRecognition>()
    val dogRecognition: LiveData<DogRecognition>
        get() = _dogRecognition


    fun recognizeImage(imageProxy: ImageProxy) {
        viewModelScope.launch {
            _dogRecognition.value = classifierRepository.reconizeImage(imageProxy)
            imageProxy.close()
        }
    }

    private fun handleResponseStatus(apiResponseStatus: ApiResponseStatus<Dog>) {
        if (apiResponseStatus is ApiResponseStatus.SUCCESS) {
            _dog.value = apiResponseStatus.data!!
        }
        _status.value = apiResponseStatus
    }

    fun getDogByMlId(mlDogId: String) {
        viewModelScope.launch {
            handleResponseStatus(dogRepository.getDogByMlId(mlDogId))

        }
    }
}